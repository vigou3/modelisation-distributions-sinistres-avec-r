%%% Copyright (C) 2021 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Modélisation des distributions de sinistres avec R»
%%% https://gitlab.com/vigou3/modelisation-distributions-sinistres-avec-r
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter[Paramétrisation des lois de probabilité]{%
  Paramétrisation des lois \\ de probabilité}
\label{chap:distributions}

Cette annexe précise la paramétrisation des lois de probabilité
continues et discrètes utilisée dans les énoncés des exercices. Dans
certains cas, elle est différente de celle présentée dans les
annexes~A et B de \cite{Klugman:lossmodels:4e:2012}. En particulier,
nous utilisons toutes les distributions de la famille gamma
transformée avec un paramètre de taux ($\lambda$) plutôt qu'un
paramètre d'échelle ($\theta$). De plus, l'ordre des paramètres est
différent.

En plus de la fonction de densité de probabilité et de la fonction de
répartition, l'annexe fournit les éléments suivants pour chaque loi:
\begin{inparaenum}[]
\item la racine \code{\textit{foo}} des fonctions
  \code{d\textit{foo}}, \code{p\textit{foo}}, \code{q\textit{foo}},
  \code{r\textit{foo}}, \code{m\textit{foo}} et \code{lev\textit{foo}}
  telles que définies dans \proglang{R} et \pkg{actuar}\footnote{%
    Les informations officielles se trouvent dans la vignette
    \emph{distributions} du paquetage \pkg{actuar}. Entrer
    \lstinline{vignette("distributions")} à la ligne de commande R
    pour consulter le document.}
\item les noms des arguments de ces fonctions correspondant à chacun
  des paramètres de la loi;
\item le $k${\ieme} moment (ainsi que l'espérance et la variance pour
  les cas les plus usuels);
\item l'espérance limitée (lois continues seulement);
\item la fonction génératrice des moments $M(t)$, lorsqu'elle existe;
\item la fonction génératrice des probabilités $P(z)$ (lois discrètes
  seulement).
\end{inparaenum}

Dans les formules ci-dessous,
\begin{displaymath}
  \Gamma(\alpha; x) = \frac{1}{\Gamma(\alpha)}
  \int_0^x t^{\alpha - 1} e^{-t}\, dt, \quad \alpha > 0,\; x > 0
\end{displaymath}
avec
\begin{displaymath}
  \Gamma(\alpha) = \int_0^\infty t^{\alpha - 1} e^{-t}\, dt
\end{displaymath}
est la fonction gamma incomplète, alors que
\begin{displaymath}
  \beta(a, b; x) = \frac{1}{\beta(a, b)}
  \int_0^x t^{a - 1} (1 - t)^{b - 1}\, dt, \quad a > 0,\; b > 0,\; 0 < x < 1
\end{displaymath}
avec
\begin{equation*}
  \beta(a, b)
  = \int_0^1 t^{a - 1} (1 - t)^{b - 1}\, dt \\
  = \frac{\Gamma(a) \Gamma(b)}{\Gamma(a + b)}
\end{equation*}
est la fonction bêta incomplète régularisée.

Sauf avis contraire, les paramètres sont strictement positifs et les
fonctions sont définies pour $x > 0$.


\section{Famille bêta transformée}
\label{sec:distributions:transformed-beta}

%% Environment itemize compact et sans puce pour cette annexe.
\begingroup
\setlist[itemize]{label={},leftmargin=0pt,align=left,nosep}

\subsection{Bêta transformée}

\begin{itemize}
  \raggedright
\item Racine: \code{trbeta}, \code{pearson6}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\gamma$),
      \code{shape3} ($\tau$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &=
  \frac{\gamma u^\tau (1 - u)^\alpha}{x \beta (\alpha, \tau )},
  \qquad u = \frac{v}{1 + v},
  \qquad v = \left(\frac{x}{\theta} \right)^\gamma \\
  F(x) &= \beta (\tau, \alpha ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(\tau + k/\gamma) \Gamma(\alpha - k/\gamma)}{%
    \Gamma(\alpha) \Gamma(\tau)},
  \quad -\tau \gamma < k < \alpha \gamma \\
  \esp{X; x} &=
  \frac{\theta \Gamma(\tau + 1/\gamma) \Gamma(\alpha - 1/\gamma)}{%
    \Gamma(\alpha) \Gamma(\tau)}
  \beta(\tau + 1/\gamma, \alpha - 1/\gamma; u) + x (1 - F(x))
\end{align*}


\subsection{Burr}

\begin{itemize}
\item Racine: \code{burr}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\gamma$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\alpha \gamma u^\alpha (1 - u)}{x},
  \qquad u = \frac{1}{1 + v},
  \qquad v = \left( \frac{x}{\theta} \right)^\gamma \\
  F(x) &= 1 - u^\alpha \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(1 + k/\gamma) \Gamma(\alpha - k/\gamma)}{%
    \Gamma(\alpha)},
  \quad -\gamma < k < \alpha \gamma \\
  \esp{X; x} &=
  \frac{\theta \Gamma(1 + 1/\gamma) \Gamma(\alpha - 1/\gamma)}{%
    \Gamma(\alpha)}
  \beta(1 + 1/\gamma, \alpha - 1/\gamma; u) + x u^\alpha
\end{align*}


\subsection{Burr inverse}

\begin{itemize}
\item Racine: \code{invburr}
\item Paramètres: \code{shape1} ($\tau$),
      \code{shape2} ($\gamma$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau \gamma u^\tau (1 - u)}{x},
  \qquad u = \frac{v}{1 + v},
  \qquad v = \left( \frac{x}{\theta} \right)^\gamma \\
  F(x) &= u^\tau \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(\tau + k/\gamma) \Gamma(1 - k/\gamma)}{%
    \Gamma(\tau)},
  \quad -\tau \gamma < k < \gamma \\
  \esp{X; x} &=
  \frac{\theta \Gamma(\tau + 1/\gamma) \Gamma(1 - 1/\gamma)}{%
    \Gamma(\alpha)}
  \beta(\tau + 1/\gamma, 1 - 1/\gamma; u) + x (1 - u^\tau)
\end{align*}

\subsection{Pareto généralisée}

\begin{itemize}
\item Racine: \code{genpareto}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\tau$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{u^\tau (1 - u)^\alpha}{x \beta (\alpha, \tau )},
  \qquad u = \frac{v}{1 + v},
  \qquad v = \frac{x}{\theta} \\
  F(x) &= \beta (\tau, \alpha ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(\tau + k) \Gamma(\alpha - k)}{%
    \Gamma(\alpha) \Gamma(\tau)},
  \quad -\tau < k < \alpha \\
  \esp{X} &=
  \frac{\theta \tau}{\alpha - 1},
  \quad \alpha > 1 \\
  \var{X} &=
  \frac{\theta^2 \tau (\tau + \alpha - 1)}{%
    (\alpha - 1)^2 (\alpha - 2)},
  \quad \alpha > 2 \\
  \esp{X; x} &=
  \frac{\theta \tau}{\alpha - 1}
  \beta(\tau + 1, \alpha - 1; u) + x (1 - F(x))
\end{align*}


\subsection{Pareto}

\begin{itemize}
\item Racine: \code{pareto}, \code{pareto2}
\item Paramètres: \code{shape} ($\alpha$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\alpha u^\alpha (1 - u)}{x},
  \qquad u = \frac{1}{1 + v},
  \qquad v = \frac{x}{\theta} \\
  F(x) &= 1 - u^\alpha \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(k + 1) \Gamma(\alpha - k)}{%
    \Gamma(\alpha)},
  \quad -1 < k < \alpha \\
  \esp{X} &=
  \frac{\theta}{\alpha - 1},
  \quad \alpha > 1 \displaybreak[0]\\
  \var{X} &=
  \frac{\theta^2 \alpha}{(\alpha - 1)^2 (\alpha - 2)},
  \quad \alpha > 2 \\
  \esp{X; x} &=
  \begin{cases}
    \dfrac{\theta}{\alpha - 1}
    \left[
      1 - \left(\frac{\theta}{x + \theta}\right)^{\alpha - 1}
    \right], & \alpha \neq 1 \\[2ex]
    -\theta \ln \left(\dfrac{\theta}{x + \theta}\right), & \alpha = 1
  \end{cases}
\end{align*}


\subsection{Pareto inverse}

\begin{itemize}
\item Racine: \code{invpareto}
\item Paramètres: \code{shape} ($\tau$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau u^\tau (1 - u)}{x},
  \qquad u = \frac{v}{1 + v},
  \qquad v = \frac{x}{\theta} \\
  F(x) &= u^\tau \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(\tau + k) \Gamma(1 - k)}{%
    \Gamma(\tau)},
  \quad -\tau < k < 1 \\
  \esp{X; x} &=
  \theta^k \tau \int_0^u \frac{y^\tau}{1 - y}\, dy +
  x (1 - u^\tau)
\end{align*}


\subsection{Log-logistique}

\begin{itemize}
\item Racine: \code{llogis}
\item Paramètres: \code{shape} ($\gamma$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\gamma u (1 - u)}{x},
  \qquad u = \frac{v}{1 + v},
  \qquad v = \left( \frac{x}{\theta} \right)^\gamma \\
  F(x) &= u \displaybreak[0]\\
  \esp{X^k} &=
  \theta^k \Gamma(1 + k/\gamma) \Gamma(1 - k/\gamma),
  \quad -\gamma < k < \gamma \\
  \esp{X; x} &=
  \theta \Gamma(1 + 1/\gamma) \Gamma(1 - 1/\gamma)
  \beta(1 + 1/\gamma, 1 - 1/\gamma; u) + x (1 - u)
\end{align*}


\subsection{Paralogistique}

\begin{itemize}
\item Racine: \code{paralogis}
\item Paramètres: \code{shape} ($\alpha$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\alpha^2 u^\alpha (1 - u)}{x},
  \qquad u = \frac{1}{1 + v},
  \qquad v = \left( \frac{x}{\theta} \right)^\alpha \\
  F(x) &= 1 - u^\alpha \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(1 + k/\alpha) \Gamma(\alpha - k/\alpha)}{%
    \Gamma(\alpha)},
  \quad -\gamma^2 < k < \alpha^2 \\
  \esp{X; x} &=
  \frac{\theta \Gamma(1 + 1/\alpha) \Gamma(\alpha - 1/\alpha)}{%
    \Gamma(\alpha)}
  \beta(1 + 1/\alpha, \alpha - 1/\alpha; u) + x u^\alpha
\end{align*}


\subsection{Paralogistique inverse}

\begin{itemize}
\item Racine: \code{invparalogis}
\item Paramètres: \code{shape} ($\tau$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau^2 u^\tau (1 - u)}{x},
  \qquad u = \frac{v}{1 + v},
  \qquad v = \left(\frac{x}{\theta} \right)^\tau \\
  F(x) &= u^\tau \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(\tau + k/\tau) \Gamma(1 - k/\tau)}{%
    \Gamma(\tau)},
  \quad -\tau^2 < k < \tau \\
  \esp{X; x} &=
  \frac{\theta \Gamma(\tau + 1/\tau) \Gamma(1 - 1/\tau)}{%
    \Gamma(\tau)}
  \beta(\tau + 1/\tau, 1 - 1/\tau; u) + x (1 - u^\tau)
\end{align*}



\section{Famille gamma transformée}
\label{sec:distributions:transformed-gamma}

\subsection{Gamma transformée}

\begin{itemize}
\item Racine: \code{trgamma}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\tau$),
      \code{rate}   ($\lambda$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau u^\alpha e^{-u}}{x \Gamma(\alpha)},
  \qquad u = (\lambda x)^\tau \\
  F(x) &= \Gamma(\alpha ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(\alpha + k/\tau)}{\lambda^k \Gamma(\alpha)},
  \quad k > -\alpha \tau \\
  \esp{X; x} &=
  \frac{\Gamma(\alpha + 1/\tau)}{\lambda \Gamma(\alpha)}\,
  \Gamma(\alpha + 1/\tau; u) + x (1 - \Gamma(\alpha; u))
\end{align*}


\subsection{Gamma transformée inverse}

\begin{itemize}
\item Racine: \code{invtrgamma}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\tau$),
      \code{rate}   ($\lambda$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau u^\alpha e^{-u}}{x\Gamma (\alpha)},
  \qquad u = (\lambda x)^{-\tau} \\
  F(x) &= 1 - \Gamma (\alpha ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(\alpha - k/\tau)}{\lambda^k \Gamma(\alpha)},
  \quad k < \alpha \tau \\
  \esp{X; x} &=
  \frac{\Gamma(\alpha - 1/\tau)}{\lambda \Gamma(\alpha)}\,
  (1 - \Gamma(\alpha - 1/\tau; u)) + x \Gamma(\alpha; u)
\end{align*}


\subsection{Gamma}

\begin{itemize}
\item Racine: \code{gamma}
\item Paramètres: \code{shape} ($\alpha$),
      \code{rate}   ($\lambda$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{u^\alpha e^{-u}}{x \Gamma(\alpha)},
  \qquad u = \lambda x \\
  F(x) &= \Gamma(\alpha ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(\alpha + k)}{\lambda^k \Gamma(\alpha)},
  \quad k > -\alpha \\
  \esp{X}
  &= \frac{\alpha}{\lambda} \\
  \var{X}
  &= \frac{\alpha}{\lambda^2} \displaybreak[0]\\
  \esp{X; x} &=
  \frac{\Gamma(\alpha + 1)}{\lambda \Gamma(\alpha)}\,
  \Gamma(\alpha + 1; u) + x (1 - \Gamma(\alpha; u)) \\
  M(t)
  &= \left( \frac{\lambda}{\lambda - t} \right)^\alpha
\end{align*}


\subsection{Gamma inverse}

\begin{itemize}
\item Racine: \code{invgamma}
\item Paramètres: \code{shape} ($\alpha$),
      \code{rate}   ($\lambda$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{u^\alpha e^{-u}}{x\Gamma (\alpha)},
  \qquad u = (\lambda x)^{-1} \\
  F(x) &= 1 - \Gamma (\alpha ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(\alpha - k)}{\lambda^k \Gamma(\alpha)},
  \quad k < \alpha \\
  \esp{X; x} &=
  \frac{\Gamma(\alpha - 1)}{\lambda \Gamma(\alpha)}\,
  (1 - \Gamma(\alpha + 1; u)) + x \Gamma(\alpha; u)
\end{align*}


\subsection{Weibull}

\begin{itemize}
\item Racine: \code{weibull}
\item Paramètres: \code{shape} ($\tau$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau u e^{-u}}{x},
  \qquad u = (\lambda x)^\tau \\
  F(x) &= 1 - e^{-u} \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(1 + k/\tau)}{\lambda^k},
  \quad k > -\tau \\
  \esp{X; x} &=
  \frac{\Gamma(1 + 1/\tau)}{\lambda}\,
  \Gamma(1 + 1/\tau; u) + x e^{-u}
\end{align*}


\subsection{Weibull inverse}

\begin{itemize}
\item Racine: \code{invweibull}, \code{lgompertz}
\item Paramètres: \code{shape} ($\tau$),
      \code{rate}   ($\lambda$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau u e^{-u}}{x},
  \qquad u = (\lambda x)^{-\tau} \\
  F(x) &= e^{-u} \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(1 - k/\tau)}{\lambda^k},
  \quad k < \tau \\
  \esp{X; x} &=
  \frac{\Gamma(1 - 1/\tau)}{\lambda}\,
  (1 - \Gamma(1 - 1/\tau; u)) + x (1 - e^{-u})
\end{align*}


\subsection{Exponentielle}

\begin{itemize}
\item Racine: \code{exp}
\item Paramètre: \code{rate}   ($\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{u e^{-u}}{x},
  \qquad u = \lambda x \\
  F(x) &= 1 - e^{-u} \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(k + 1)}{\lambda^k},
  \quad k > -1 \\
  \esp{X}
  &= \frac{1}{\lambda} \displaybreak[0]\\
  \var{X}
  &= \frac{1}{\lambda^2} \displaybreak[0]\\
  \esp{X; x} &=
  \frac{1 - e^{-u}}{\lambda} \\
  M(t)
  &= \frac{\lambda}{\lambda - t}
\end{align*}


\subsection{Exponentielle inverse}

\begin{itemize}
\item Racine: \code{invexp}
\item Paramètres: \code{rate}   ($\lambda$),
      \code{scale}  ($\theta = 1/\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{u e^{-u}}{x},
  \qquad u = (\lambda x)^{-1} \\
  F(x) &= e^{-u} \\
  \esp{X^k} &=
  \frac{\Gamma(1 - k)}{\lambda^k},
  \quad k < 1
\end{align*}


\section{Autres distributions continues}
\label{sec:distributions:other}

\subsection{Normale}

\begin{itemize}
\item Racine: \code{norm}
\item Paramètres: \code{mean} ($-\infty < \mu < \infty$),
      \code{sd}   ($\sigma$)
\end{itemize}
\begin{align*}
  f(x)
  &= \frac{1}{\sqrt{2\pi} \sigma}\,
  \exp\bigg\{
    - \frac{1}{2} \left(\frac{x - \mu}{\sigma}\right)^2
  \bigg\}, \quad
  -\infty < x < \infty \\
  F(x)
  &= \Phi\left( \frac{x - \mu}{\sigma} \right), \quad
  \Phi(x) = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^x e^{-y^2}\, dy \displaybreak[0]\\
  \esp{X}
  &= \mu \\
  \var{X}
  &= \sigma^2 \\
  M(t)
  &= e^{\mu t + \sigma^2 t^2/2}
\end{align*}


\subsection{Log-normale}

\begin{itemize}
\item Racine: \code{lnorm}
\item Paramètres: \code{meanlog} ($\alpha$),
      \code{sdlog}   ($\sigma$)
\end{itemize}
\begin{align*}
  f(x)
  &= \frac{1}{\sqrt{2\pi} \sigma} \frac{1}{x}\,
  \exp\bigg\{
    - \frac{1}{2} \left(\frac{\ln x - \mu}{\sigma}\right)^2
  \bigg\} \\
  F(x)
  &= \Phi\left( \frac{\ln x - \mu}{\sigma} \right) \displaybreak[0]\\
  \esp{X^k}
  &= e^{k \mu + k^2 \sigma^2/2} \displaybreak[0]\\
  \esp{X}
  &= e^{\mu + \sigma^2/2} \\
  \var{X}
  &= e^{2\mu + \sigma^2}(e^{\sigma^2} - 1) \\
  \esp{X; x}
  &= e^{\mu + \sigma^2/2}\,
  \Phi \left(\frac{\ln x - \mu - \sigma^2}{\sigma}\right)
  + x \Bigg[
  1 - \Phi \left( \frac{\ln x - \mu}{\sigma} \right)
  \Bigg]
\end{align*}


\subsection{Log-gamma}

\begin{itemize}
\item Racine: \code{lgamma}
\item Paramètres: \code{shapelog} ($\alpha$),
      \code{ratelog}   ($\lambda$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\lambda^\alpha (\ln x)^{\alpha - 1}}{%
    x^{\lambda + 1} \Gamma(\alpha)},
  \qquad x > 1 \\
  F(x) &= \Gamma( \alpha ; \lambda \ln x), \qquad x > 1 \displaybreak[0]\\
  \esp{X^k}
  &= \left( \frac{\lambda}{\lambda - k} \right)^\alpha \\
  \esp{X}
  &= \left( \frac{\lambda}{\lambda - 1} \right)^\alpha \\
  \var{X}
  &= \left( \frac{\lambda}{\lambda - 2} \right)^\alpha -
  \left( \frac{\lambda}{\lambda - 1} \right)^{2 \alpha} \\
  \esp{X; x} &=
  \left( \frac{\lambda}{\lambda - 1} \right)^\alpha
  \Gamma(\alpha; (\lambda - 1) \ln x) +
  x (1 - \Gamma(\alpha; \lambda \ln x))
\end{align*}


\subsubsection{Gumbel}

\begin{itemize}
\item Racine: \code{gumbel}
\item Paramètres: \code{alpha} ($-\infty < \alpha < \infty$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x)
  &= \frac{e^{-(u + e^{-u})}}{\theta},
    \qquad u = \frac{x - \alpha}{\theta},
    \qquad -\infty < x < \infty \\
  F(x)
  &= \exp[-\exp(-u)] \\ \displaybreak[0]
  \esp{X}
  &= \alpha + \gamma \theta, \qquad \gamma \approx 0.57721566490153 \\
  \var{X}
  &= \frac{\pi^2 \theta^2}{6} \\
  M(t)
  &= e^{\alpha t} \Gamma(1 - \theta t)
\end{align*}

\subsubsection{Inverse gaussienne}

\begin{itemize}
\item Racine: \code{invgauss}
\item Paramètres: \code{mean} ($\mu$),
  \code{shape} ($\lambda = 1/\phi$),
  \code{dispersion} ($\phi$)
\end{itemize}
\begin{align*}
  f(x)
  &= \left( \frac{1}{2 \pi \phi x^3} \right)^{1/2}
    \exp\left\{ -\frac{(x/\mu - 1)^2}{2 \phi \mu^2 x} \right\} \\
  F(x)
  &= \Phi\left( \frac{x/\mu - 1}{\sqrt{\phi x}} \right)
    + e^{2/(\phi\mu)}
    \Phi\left( -\frac{x/\mu + 1}{\sqrt{\phi x}} \right) \\ \displaybreak[0]
  \esp{X^k}
  &= \mu^k \sum_{i = 0}^{k - 1} \frac{(k + i - 1)!}{i! (k - i - 1)!}
    \left( \frac{\phi \mu}{2} \right)^{i} \\
  \esp{X \wedge x}
  &= \mu
    \left[
    \Phi\left( \frac{x/\mu - 1}{\sqrt{\phi x}} \right)
    - e^{2/(\phi\mu)} \Phi\left(- \frac{x/\mu + 1}{\sqrt{\phi x}} \right)
    \right] \\
  &\phantom{=} + x (1 - F(x)) \\
  M(t)
  &= \exp
    \left\{
      \frac{1}{\phi \mu} \left(1 - \sqrt{1 - 2 \phi \mu^2 t}\right)
    \right\},
    \qquad t \leq \frac{1}{2 \phi \mu^2}
\end{align*}

\noindent%
Le cas limite $\mu = \infty$ est une distribution inverse gamma avec
$\alpha = 1/2$ et $\lambda = 2\phi$ (ou inverse khi carré).


\subsection{Pareto translatée}

\begin{itemize}
\item Racine: \code{pareto1}
\item Paramètres: \code{shape} ($\alpha$),
      \code{min}   ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\alpha
    \theta^\alpha}{x^{\alpha+1}}, \qquad x > \theta \\
  F(x) &= 1 - \left( \frac{\theta}{x} \right)^\alpha, \qquad x >
  \theta \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\alpha \theta^k}{\alpha - k},
  \quad k < \alpha \\
  \esp{X; x} &=
  \frac{\alpha \theta}{\alpha - 1} -
  \frac{\theta}{(\alpha - 1) x^{\alpha - 1}}
\end{align*}

\noindent Cette loi est également appelée Pareto à un paramètre. Seul
$\alpha$ est considéré comme un véritable paramètre de la
distribution. Le paramètre $\theta$ est la borne inférieure du support
de la distribution et est en général considéré connu.


\subsection{Bêta généralisée}

\begin{itemize}
  \raggedright
\item Racine: \code{genbeta}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\beta$),
      \code{shape3} ($\tau$),
      \code{rate}   ($\lambda = 1/\theta$),
      \code{scale}  ($\theta$)
\end{itemize}
\begin{align*}
  f(x) &= \frac{\tau u^\alpha (1 - u)^{\beta - 1}}{x \beta (\alpha, \beta)},
  \qquad u = \left( \frac{x}{\theta} \right)^\tau,
  \qquad 0 < x < \theta \\
  F(x) &= \beta (\alpha, \beta ; u) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\theta^k \Gamma(\alpha + \beta) \Gamma(\alpha + k/\tau)}{%
    \Gamma(\alpha) \Gamma(\alpha + \beta + k/\tau)},
  \quad k > -\alpha \tau \\
  \esp{X; x} &=
  \frac{\theta \Gamma(\alpha + \beta) \Gamma(\alpha + 1/\tau)}{%
    \Gamma(\alpha) \Gamma(\alpha + \beta + 1/\tau)}\,
  \beta(\alpha + 1/\tau, \beta; u) + x (1 - \beta(\alpha, \beta; u))
\end{align*}


\subsection{Bêta}

\begin{itemize}
\item Racine: \code{beta}
\item Paramètres: \code{shape1} ($\alpha$),
      \code{shape2} ($\beta$)
\end{itemize}
\begin{align*}
  f(x)
  &= \frac{\Gamma(\alpha + \beta)}{\Gamma(\alpha) \Gamma(\beta)}\,
  x^{\alpha - 1} (1 - x)^{\beta - 1}, \quad 0 < x < 1 \\
  F(x) &= \beta(\alpha, \beta; x) \displaybreak[0]\\
  \esp{X^k} &=
  \frac{\Gamma(\alpha + \beta) \Gamma(\alpha + k)}{%
    \Gamma(\alpha) \Gamma(\alpha + \beta + k)},
  \quad k > -\alpha \\
  \esp{X}
  &= \frac{\alpha}{\alpha + \beta} \\
  \var{X}
  &= \frac{\alpha \beta}{(\alpha + \beta)^2 (\alpha + \beta + 1)} \\
  \esp{X; x} &=
  \frac{\Gamma(\alpha + \beta) \Gamma(\alpha + 1)}{%
    \Gamma(\alpha) \Gamma(\alpha + \beta + 1)}\,
  \beta(\alpha + 1, \beta; u) + x (1 - \beta(\alpha, \beta; x))
\end{align*}



\section{Distributions discrètes de la famille $(a, b, 0)$}
\label{sec:distributions:discretes}


\subsection{Binomiale}

\begin{itemize}
\item Racine: \code{binom}
\item Paramètres: \code{size} ($n$),
      \code{prob} ($\theta$)
\end{itemize}
\begin{align*}
  \prob{X = x} &=
  \binom{n}{x} \theta^x (1 - \theta)^{n - x}, \quad
  n \text{ entier},\; 0 < \theta < 1,\; x = 0, 1, \dots, n \\
  \esp{X} &= n \theta \\
  \var{X} &= n \theta (1 - \theta) \\
  M(t) &= (1 - \theta + \theta e^t)^n \\
  P(z) &= (1 - \theta(z - 1))^n
\end{align*}


\subsection{Binomiale négative}

\begin{itemize}
\item Racine: \code{nbinom}
\item Paramètres: \code{size} ($r$),
      \code{prob} ($\theta$),
      \code{mu} ($\mu = r (1 - \theta)/\theta$)
\end{itemize}
\begin{align*}
  \prob{X = x} &=
  \binom{x + r - 1}{r - 1} \theta^r (1 - \theta)^x, \quad
  0 < \theta < 1,\; x = 0, 1, \dots \displaybreak[0] \\
  \esp{X} &= \frac{r (1 - \theta)}{\theta} \\
  \var{X} &= \frac{r (1 - \theta)}{\theta^2} \\
  M(t) &= \left( \frac{\theta}{1 - (1 - \theta)e^t} \right)^r \\
  P(z) &= (1 - (1 - \theta) z)^{-r}
\end{align*}


\subsection{Géométrique}

\begin{itemize}
\item Racine: \code{nbinom}
\item Paramètre: \code{prob} ($\theta$)
\end{itemize}
\begin{align*}
  \prob{X = x} &=
  \theta (1 - \theta)^x, \quad
  0 < \theta < 1,\; x = 0, 1, \dots \displaybreak[0]\\
  \esp{X} &= \frac{1 - \theta}{\theta} \\
  \var{X} &= \frac{1 - \theta}{\theta^2} \displaybreak[0]\\
  M(t) &= \frac{\theta}{1 - (1 - \theta)e^t} \\
  P(z) &= (1 - (1 - \theta) z)^{-1}
\end{align*}


\subsection{Poisson}

\begin{itemize}
\item Racine: \code{pois}
\item Paramètre: \code{lambda} ($\lambda$)
\end{itemize}
\begin{align*}
  \prob{X = x}
  &= \frac{\lambda^x e^{-\lambda}}{x!}, \quad x = 0, 1, \dots \\
  \esp{X} &= \lambda \\
  \var{X} &= \lambda \\
  M(t) &= e^{\lambda (e^t - 1)} \\
  P(z) &= e^{\lambda (z - 1)}
\end{align*}

\endgroup                       % limite la portée de \setlist

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "modelisation-distributions-sinistres-avec-r"
%%% coding: utf-8
%%% TeX-engine: xetex
%%% End:

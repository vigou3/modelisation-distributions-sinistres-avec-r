%%% Copyright (C) 2021 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Modélisation des distributions de sinistres avec R»
%%% https://gitlab.com/vigou3/modelisation-distributions-sinistres-avec-r
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}
\markboth{Introduction}{Introduction}

La modélisation statistique des montants et de la fréquence des
sinistres joue un rôle crucial dans l'évaluation quantitative des
risques en assurance de dommages. Ce document offre un traitement
exhaustif du sujet, et ce, tant d'un point de vue théorique que dans
les aspects numériques. Il s'agit de mes notes du cours Mathématiques
actuarielles IARD~I offert à l'École d'actuariat de l'Université Laval
auxquelles j'ai intégré du code informatique ainsi que le recueil
d'exercices de \citet{Cossette:lossdist:2009}.

Les notes et les exercices de cet ouvrage ont atteint un niveau de
maturité enviable. Si c'est le cas, c'est parce que j'ai eu le
privilège de bâtir sur du matériel de grande qualité développé au fil
des ans par les professeurs Hélène Cossette, François Dufresne, José
Garrido, Michel Jacques et Jacques Rioux, ainsi que par le chargé de
cours Mathieu Pigeon alors qu'il étudiait à la maîtrise en actuariat
au milieu des années 2000.

La matière est divisée en sept chapitres. Le
\hyperref[chap:rappels]{premier} est un chapitre de rappels des
notions de base en analyse, en probabilité et en statistique. Le
\autoref{chap:modelisation} traite des fondements de la modélisation
en assurance de dommages, en particulier le traitement mathématique
des franchises, limite supérieure et coassurance, ainsi que de l'effet
de l'inflation sur la fréquence et la sévérité des sinistres. Les
aspects plus statistiques apparaissent au \autoref{chap:nparam} avec
la modélisation non paramétrique. Le \autoref{chap:modeles} étudie les
principales distributions utilisées en assurance de dommages et la
création de nouvelles distributions à partir des lois usuelles. Les
chapitres~\ref{chap:param} et \ref{chap:tests} portent quant à eux sur
l'estimation paramétrique et les tests d'adéquation des modèles.
Enfin, le \autoref{chap:frequence} propose une brève incursion dans la
modélisation des distributions de fréquence des sinistres.

Les termes anglais \emph{ordinary deductible} et \emph{franchise
  deductible} utilisés dans les textes de référence usuels en
actuariat ont posé quelques soucis de traduction. Pour le premier,
j'utilise l'expression «franchise forfaitaire» recommandée par
\cite{LGA}. Pour le second terme, beaucoup moins répandu, j'ai opté
pour l'expression «franchise atteinte» suggérée, entre autres, dans
\cite{Charbonnier:dictionnaire:2004}.

La présentation fait une large place aux considérations numériques
auxquelles tout actuaire praticien se retrouvera rapidement confronté.
Parce qu'il s'agit du meilleur outil aujourd'hui disponible pour faire
l'analyse, le traitement et la modélisation de données de sinistres,
les exemples et illustrations sont entièrement réalisés dans
l'environnement statistique \proglang{R} \citep{R}. Nous aurons
également recours aux fonctionnalités additionnelles du paquetage
\pkg{actuar} \citep{actuar}. Plusieurs fonctions de ce paquetage ont
d'ailleurs été développées à l'origine spécifiquement pour le présent
matériel.

Chaque chapitre comporte des exercices. Les réponses se trouvent à la
fin des chapitres et les solutions complètes, à
l'\autoref{chap:solutions}. Vous trouverez également à la fin de
chaque chapitre (sauf le premier) une liste (non exhaustive) d'exercices
suggérés dans \citet{Klugman:lossmodels:4e:2012}. Des solutions de ces
exercices sont offertes dans \citet{Klugman:solutions:4e:2012}.

L'\autoref{chap:distributions} présente la paramétrisation des lois
de probabilité continues et discrètes utilisée dans les exercices.
L'information qui s'y trouve est en plusieurs points similaire à celle
des annexes~A et B de \citet{%
  Klugman:lossmodels:3e:2008,%
  Klugman:lossmodels:4e:2012}, %
mais la paramétrisation des lois est dans certains cas différente. Le
lecteur est donc fortement invité à la consulter.

L'\autoref{chap:paquetages} explique comment configurer \proglang{R}
pour faciliter l'installation et l'administration de paquetages
externes. Enfin, les annexes~\ref{chap:normale} et \ref{chap:khi2}
offrent des tables de quantiles des lois normale et khi~carré.

\section*{Utilisation de l'ouvrage}

L'étude de l'ouvrage implique des allers-retours entre le texte et le
code R à la fin des chapitres. Ce code informatique et les
commentaires qui l'accompagnent vise à enrichir vos apprentissages.
Assurez-vous donc de le lire attentivement, de l'exécuter pas-à-pas et
de bien comprendre ses effets.

Le code informatique est distribué avec l'ouvrage sous forme de
fichiers de script. De plus, à chaque fichier \code{.R} correspond un
fichier \code{.Rout} contenant les résultats de son évaluation non
interactive.

\section*{Fonctionnalités interactives}

En consultation électronique, ce document se trouve enrichi de
plusieurs fonctionnalités interactives.
\begin{itemize}
\item Intraliens du texte vers une ligne précise d'une section de code
  informatique et, en sens inverse, du numéro de la ligne vers le
  point de la référence dans le texte. Ces intraliens sont marqués par
  la couleur \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre le numéro d'un exercice et sa solution, et vice
  versa. Ces intraliens sont aussi marqués par la couleur
  \textcolor{link}{\rule{1.5em}{1.2ex}}.
\item Intraliens entre les citations dans le texte et leur entrée dans
  la bibliographie. Ces intraliens sont marqués par la couleur
  \textcolor{citation}{\rule{1.5em}{1.2ex}}.
\item Hyperliens vers des ressources externes marqués par le symbole
  {\smaller\faExternalLink*} et la couleur
  \textcolor{url}{\rule{1.5em}{1.2ex}}.
\item Table des matières, liste des tableaux et liste des figures
  permettant d'accéder rapidement à des ressources du document.
\end{itemize}

\section*{Blocs signalétiques}

Le document est parsemé de divers types de blocs signalétiques
inspirés de
\link{https://asciidoctor.org/docs/user-manual/\#admonition}{AsciiDoc}
qui visent à attirer votre attention sur une notion. Vous pourrez
rencontrer l'un ou l'autre des blocs suivants.

\tipbox{Astuce! Ces blocs contiennent un truc, une astuce, ou tout
  autre type d'information utile.}
\vspace{-\baselineskip}

\warningbox{Avertissement! Ces blocs mettent l'accent sur une notion
  ou fournissent une information importante pour la suite.}
\vspace{-\baselineskip}

\importantbox{Important! Ces blocs contiennent les remarques les plus
  importantes. Veillez à en tenir compte.}
\vspace{-\baselineskip}

\gotorbox{Ces blocs vous invitent à interrompre la lecture du texte
  pour passer à l'étude du code R des sections d'exemples.}
\vspace{-\baselineskip}

\section*{Document libre}

Tout comme R et l'ensemble des outils présentés dans ce document, le
projet «Modélisation des distributions de sinistres avec R» s'inscrit
dans le mouvement de
l'\link{https://www.gnu.org/philosophy/free-sw.html}{informatique
  libre}. Vous pouvez accéder à l'ensemble du code source en format
{\LaTeX} en suivant le lien dans la page de copyright. Vous trouverez
dans le fichier \code{README.md} toutes les informations utiles pour
composer le document.

Votre contribution à l'amélioration du document est également la
bienvenue; consultez le fichier \code{CONTRIBUTING.md} fourni avec ce
document et voyez votre nom ajouté au fichier \code{COLLABORATEURS}.

\section*{Remerciements}

Je remercie Philippe Cleary pour ses suggestions sur le texte, ainsi
que Simon Youde et Étienne Vanasse pour m'avoir signalé plusieurs
coquilles dans une édition précédente du document.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "modelisation-distributions-sinistres-avec-r"
%%% TeX-engine: xetex
%%% End:

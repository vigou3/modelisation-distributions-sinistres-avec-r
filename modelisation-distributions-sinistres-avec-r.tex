%%% Copyright (C) 2021 Vincent Goulet
%%%
%%% Ce fichier et tous les fichiers .tex ou .Rnw dont la racine est
%%% mentionnée dans les commandes \include ci-dessous font partie du
%%% projet «Modélisation des distributions de sinistres avec R».
%%% https://gitlab.com/vigou3/modelisation-distributions-sinistres-avec-r
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[letterpaper,11pt,x11names,english,french]{memoir}
  \usepackage{natbib,url}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath,amsthm}
  \usepackage[noae]{Sweave}
  \usepackage{graphicx}
  \usepackage{actuarialsymbol}         % symboles actuariels
  \usepackage{currfile}                % noms des fichiers de script
  \usepackage{framed}                  % env. snugshade*, oframed
  \usepackage{paralist}                % exercices
  \usepackage[absolute]{textpos}       % disposition d'images
  \usepackage[shortlabels]{enumitem}   % configuration listes
  \usepackage{relsize}                 % \smaller et al.
  \usepackage{manfnt}                  % \mantriangleright (puce)
  \usepackage{metalogo}                % \XeLaTeX logo
  \usepackage{fontawesome5}            % plusieurs icônes
  \usepackage{awesomebox}              % boites signalétiques
  \usepackage{answers}                 % exercices et solutions
  \usepackage{listings}                % code informatique
  \usepackage{refcount}                % numéros de ligne en surbrillance

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{Modélisation des distributions de sinistres avec R}
  \author{Vincent Goulet}
  \renewcommand{\year}{2021}
  \renewcommand{\month}{05}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/modelisation-distributions-sinistres-avec-r/}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Polices de caractères
  \usepackage{fontspec}
  \usepackage{unicode-math}
  \defaultfontfeatures
  {
    Scale = 0.92
  }
  \setmainfont{Lucida Bright OT}
  [
    Ligatures = TeX,
    Numbers = OldStyle
  ]
  \setmathfont{Lucida Bright Math OT}
  \setmonofont{Lucida Grande Mono DK}
  \setsansfont{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    ItalicFont = *-BookItalic,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 0.98,
    Numbers = OldStyle
  ]
  \newfontfamily\fullcaps{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    Scale = 0.98,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Couleurs
  \usepackage{xcolor}
  \definecolor{comments}{rgb}{0.5,0.55,0.6} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % liens internes
  \definecolor{url}{rgb}{0.6,0,0}           % liens externes
  \definecolor{citation}{rgb}{0,0.5,0}      % citations
  \colorlet{codebg}{LightYellow1}           % fond code R
  \colorlet{lineno}{gray}                   % numéros de lignes
  \colorlet{TFFrameColor}{black}            % encadrés (cadre)
  \colorlet{TFTitleColor}{white}            % encadrés (titre)
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % rouge bandeau identitaire
  \definecolor{or}{rgb}{1,0.8,0}            % or bandeau identitaire
  \colorlet{shadecolor}{black}

  %% Hyperliens
  \usepackage[backref=page]{hyperref}
  \hypersetup{%
    pdfauthor = \theauthor,
    pdftitle = \thetitle,
    colorlinks = true,
    linktocpage = true,
    urlcolor = {url},
    linkcolor = {link},
    citecolor = {citation},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}
  \renewcommand*{\backrefalt}[4]{%
    \ifcase #1 %
      Aucune citation.%
    \or
      Cité à la page #2.%
    \else
      Cité aux pages #2.%
    \fi
  }
  \setlength{\XeTeXLinkMargin}{1pt}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Étiquettes de \autoref (redéfinitions compatibles avec babel).
  %% Attention! Les % à la fin des lignes sont importants sinon des
  %% blancs apparaissent dès que la commande \selectlanguage est
  %% utilisée, notamment dans la bibliographie.
  \addto\extrasfrench{%
    \def\subsectionautorefname{section}%
    \def\figureautorefname{figure}%
    \def\tableautorefname{tableau}%
    \def\thmautorefname{théorème}%
    \def\exempleautorefname{exemple}%
    \def\definitionautorefname{définition}%
    \def\exerciceautorefname{exercice}%
    \def\appendixautorefname{annexe}%
  }

  %% Table des matières et al. (inspiré de classicthesis.sty)
  \renewcommand{\cftchapterleader}{\hspace{1.5em}}
  \renewcommand{\cftchapterafterpnum}{\cftparfillskip}
  \renewcommand{\cftsectionleader}{\hspace{1.5em}}
  \renewcommand{\cftsectionafterpnum}{\cftparfillskip}
  \renewcommand{\cfttableleader}{\hspace{1.5em}}
  \renewcommand{\cfttableafterpnum}{\cftparfillskip}
  \renewcommand{\cftfigureleader}{\hspace{1.5em}}
  \renewcommand{\cftfigureafterpnum}{\cftparfillskip}
  \setlength{\cftbeforechapterskip}{1.0em plus 0.5em minus 0.5em}
  \setlength{\cftfigurenumwidth}{3.2em}

  %% Titres des chapitres
  \chapterstyle{hangnum}
  \renewcommand{\chaptitlefont}{\normalfont\Huge\sffamily\bfseries\raggedright}

  %% Marges, entêtes et pieds de page
  \setlength{\marginparsep}{7mm}
  \setlength{\marginparwidth}{13mm}
  \setlength{\headwidth}{\textwidth}
  \addtolength{\headwidth}{\marginparsep}
  \addtolength{\headwidth}{\marginparwidth}

  %% Titres des sections et sous-sections
  \setsecheadstyle{\normalfont\Large\sffamily\bfseries\raggedright}
  \setsubsecheadstyle{\normalfont\large\sffamily\bfseries\raggedright}
  \maxsecnumdepth{subsection}
  \setsecnumdepth{subsection}

  %% Listes. Paramétrage avec enumitem.
  \setlist[enumerate]{leftmargin=*,align=left}
  \setlist[enumerate,2]{label=\alph*),labelsep=*,leftmargin=1.5em}
  \setlist[enumerate,3]{label=\roman*),labelsep=*,leftmargin=1.5em,align=right}
  \setlist[itemize]{leftmargin=*,align=left}
  \setlist[trivlist]{nosep}

  %% Options de babel
  \frenchbsetup{CompactItemize=false,%
    ThinSpaceInFrenchNumbers=true,
    ItemLabeli=\mantriangleright,
    ItemLabelii=\textendash,
    og=«, fg=»}
  \addto\captionsfrench{\def\figurename{{\scshape Fig.}}}
  \addto\captionsfrench{\def\tablename{{\scshape Tab.}}}
  \addto\captionsfrench{\def\listfigurename{Liste des figures}}

  %%% =========================
  %%%  Sections de code source
  %%% =========================

  %% Syntaxe de R avec ajouts de quelques mots clés.
  \lstloadlanguages{R}
  \lstdefinelanguage{Renhanced}[]{R}{%
    otherkeywords = {!,!=,~,$,*,\&,\%/\%,\%*\%,\%\%,<-,<<-},%$
    morekeywords={colMeans,colSums,head,is.na,is.null,mapply,ms,na.rm,%
      nlmin,rep_len,replicate,row.names,rowMeans,rowSums,seq_len,%
      seq_along,sys.time,system.time,tail,which.max,which.min,letters,%
      LETTERS,Trig},
    deletekeywords={c,start,q,_},
    alsoletter={._\%},
    alsoother={:\$},
    moredelim=[il]{\#-*-}}

  %% Mise en forme du code source.
  %%
  %% Les numéros de lignes sont des hyperliens vers le point dans le
  %% document où l'on y fait référence dans une boite \gotorbox (voir
  %% plus loin).
  %%
  %% Pour y parvenir, j'utilise deux étiquettes pour une ligne: une
  %% basée sur un "nom" utilisé dans la rédaction, et une autre
  %% générée automatiquement à partir du numéro de chapitre et du
  %% numéro de ligne.
  %%
  %% Solution basée sur https://tex.stackexchange.com/q/191771
  \lstset{%
    language=Renhanced,
    extendedchars=true,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    showstringspaces=false,
    numbers=left,
    numberstyle={%
      \color{lineno}\tiny\ttfamily%
      \ifnum\value{lstnumber}=\getrefnumber{code:\thechapter:\thelstnumber}%
        \renewcommand*\thelstnumber{\hyperlink{goto:\thechapter:\the\value{lstnumber}}{\bfseries\arabic{lstnumber}}}%
      \fi},
    firstnumber=\scriptfirstline,
    escapechar=`}

  %% Commandes pour créer les références et liens vers des numéros de
  %% lignes.
  \makeatletter
  \newcommand\labelline[1]{%
    \def\@currentlabel{\thelstnumber}%
    \label{lst:#1}\label{code:\thechapter:\the\value{lstnumber}}}
  \makeatother
  \newcommand{\reflines}[1]{%
    \hypertarget{goto:\thechapter:\getrefnumber{lst:#1}}{\ref{lst:#1}}--%
    \hypertarget{goto:\thechapter:\getrefnumber{lst:#1:fin}}{\ref{lst:#1:fin}}}

  %% L'entête des fichiers de script n'est pas affiché dans le
  %% document.
  \def\scriptfirstline{11}      % nombre magique!

  %%% =========================
  %%%  Nouveaux environnements
  %%% =========================

  %% Environnements d'exemples et al.
  \theoremstyle{plain}
  \newtheorem{thm}{Théorème}[chapter]
  \newtheorem{corr}{Corollaire}[chapter]

  \theoremstyle{definition}
  \newtheorem{exemple}{Exemple}[chapter]
  \newtheorem{definition}{Définition}[chapter]

  %% Redéfinition de l'environnement titled-frame de framed.sty avec
  %% deux modifications: épaisseur des filets réduite de 2pt à 1pt;
  %% "(suite)" plutôt que "(cont)" dans la barre de titre
  %% lorsque l'encadré se poursuit après un saut de page.
  \renewenvironment{titled-frame}[1]{%
    \def\FrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame{\textbf{#1}}}%
    \def\FirstFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame[$\blacktriangleright$]{\textbf{#1}}}%
    \def\MidFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame[$\blacktriangleright$]{\textbf{#1\ (suite)}}}%
    \def\LastFrameCommand{\fboxsep8pt\fboxrule1pt
      \TitleBarFrame{\textbf{#1\ (suite)}}}%
    \MakeFramed{\advance\hsize-16pt \FrameRestore}}%
  {\endMakeFramed}

  %% Liste d'objectifs au début des chapitres dans un encadré
  %% basé sur titled-frame, ci-dessus.
  \newenvironment{objectifs}{%
    \begin{titled-frame}{\rule[-7pt]{0pt}{20pt}\sffamily Objectifs du chapitre}
      \setlength{\parindent}{0pt}
      \small\sffamily
      \begin{itemize}[nosep]}%
      {\end{itemize}\end{titled-frame}}

  %% Environnements de Sweave. Les environnements Sinput et Soutput
  %% utilisent Verbatim (de fancyvrb). On les réinitialise pour
  %% enlever la configuration par défaut de Sweave, puis on réduit
  %% l'écart entre les blocs Sinput et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.sty.
  \makeatletter
  \renewenvironment{Schunk}{%
    \setlength{\topsep}{1pt}
    \def\FrameCommand##1{\hskip\@totalleftmargin
       \vrule width 2pt\colorbox{codebg}{\hspace{3pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Exercices et réponses
  \Newassociation{sol}{solution}{solutions}
  \Newassociation{rep}{reponse}{reponses}
  \newcounter{exercice}[chapter]
  \renewcommand{\theexercice}{\thechapter.\arabic{exercice}}
  \newenvironment{exercice}{%
    \begin{list}{}{%
        \refstepcounter{exercice}
        \hypertarget{ex:\theexercice}{}
        \Writetofile{solutions}{\protect\hypertarget{sol:\theexercice}{}}
        \renewcommand{\makelabel}{%
          \bfseries\protect\hyperlink{sol:\theexercice}{\theexercice}}
        \settowidth{\labelwidth}{\bfseries \thechapter.\arabic{exercice}}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
      \item}
    {\end{list}}
  \renewenvironment{solution}[1]{%
    \begin{list}{}{%
        \renewcommand{\makelabel}{%
          \bfseries\protect\hyperlink{ex:#1}{#1}}
        \settowidth{\labelwidth}{\bfseries #1}
        \setlength{\leftmargin}{\labelwidth}
        \addtolength{\leftmargin}{\labelsep}
        \setlist[enumerate,1]{label=\alph*),labelsep=*,leftmargin=1.5em}
        \setlist[enumerate,2]{label=\roman*),labelsep=0.5em,align=right}}
      \item}
    {\end{list}}
  \renewenvironment{reponse}[1]{%
    \begin{enumerate}[label=\textbf{#1}]
      \item}
    {\end{enumerate}}

  %% Corriger un bogue dans answers apparu dans TeX Live 2020.
  %% https://tex.stackexchange.com/a/537873
  \makeatletter
  \renewcommand{\Writetofile}[2]{%
    \@bsphack
    \Iffileundefined{#1}{}{%
      \Ifopen{#1}{%
        {%
          \begingroup
          % \let\protect\string %%% <----- removed
          \Ifanswerfiles{%
            \protected@iwrite{\@nameuse{#1@file}}{\afterassignment\let@protect@string}{#2}%
          }{}%
          \endgroup
        }%
      }{}%
    }%
    \@esphack
  }
  \def\let@protect@string{\let\protect\string}
  \makeatother

  %%% ====================
  %%%  Nouvelles commandes
  %%% =====================

  %% Noms de fonctions, code, etc.
  \newcommand{\code}[1]{\texttt{#1}}
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\proglang}[1]{#1}

  %% Opérateurs mathématiques
  \newcommand{\esp}[1]{E[ #1 ]}
  \newcommand{\hesp}[1]{\hat{E}[ #1 ]}
  \newcommand{\Esp}[1]{E\!\left[ #1 \right]}
  \DeclareMathOperator{\VAR}{Var}
  \newcommand{\var}[1]{\VAR[ #1 ]}
  \newcommand{\hvar}[1]{\widehat{\VAR}[ #1 ]}
  \newcommand{\Var}[1]{\VAR\! \left[ #1 \right]}
  \newcommand{\hVar}[1]{\widehat{\VAR}\! \left[ #1 \right]}
  \DeclareMathOperator{\Cov}{Cov}
  \newcommand{\abs}[1]{\lvert #1 \rvert}
  \newcommand{\norme}[1]{\lVert #1 \rVert}
  \newcommand{\Prob}[1]{\Pr\! \left[ #1 \right]}
  \newcommand{\prob}[1]{\Pr[ #1 ]}
  \newcommand{\mat}[1]{\symbf{#1}}
  \newcommand{\bm}[1]{\symbf{#1}} % commande de unicode-math
  \newcommand{\LER}{\text{LER}}
  \newcommand{\MSE}{\text{MSE}}
  \newcommand{\R}{\mathbb{R}}
  \newcommand{\N}{\mathbb{N}}
  \newcommand{\Z}{\mathbb{Z}}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\raisebox{-0.1ex}{\smaller\faExternalLink*}}%
    \else
      \href{#1}{#2~\raisebox{-0.1ex}{\smaller\faExternalLink*}}%
    \fi
  }

  %% Boite additionnelle (basée sur awesomebox.sty) pour changements
  %% au fil de la lecture.
  \newcommand{\gotorbox}[1]{%
    \awesomebox{\aweboxrulewidth}{\faMapSigns}{black}{#1}}

  %% Boite pour le nom du fichier de script correspondant au début des
  %% sections d'exemples.
  \newcommand{\scriptfile}[1]{%
    \begingroup
    \noindent
    \mbox{%
      \makebox[3mm][l]{\raisebox{-0.5pt}{\small\faChevronCircleDown}}\;%
      \smaller[1] {\sffamily Fichier d'accompagnement} {\ttfamily #1}}
    \endgroup}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Sous-figures
  \newsubfloat{figure}

  %% Un petit peu d'aide pour la césure
  \hyphenation{logis-ti-que}

  %% Style de la bibliographie
  \bibliographystyle{francais}

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière
  \newlength{\imageheight}
  \newlength{\banderougewidth}
  \newlength{\banderougeheight}
  \newlength{\bandeorwidth}
  \newlength{\bandeorheight}
  \newlength{\logoheight}
  \newlength{\gapwidth}

%  \includeonly{notices}

\begin{document}

\frontmatter

%% Page couverture avant
\pagestyle{empty}
\input{couverture-avant}
\null\cleardoublepage           % cf. section 2.2 textpos.pdf

%% Page de copyright
\include{notices}
\clearpage

%% Corps du document
\pagestyle{companion}

\include{introduction}

\tableofcontents
\cleartorecto
\listoftables
\cleartorecto
\listoffigures

\mainmatter

\include{rappels}
\include{modelisation}
\include{nparam}
\include{modeles}
\include{param}
\include{tests}
\include{frequence}

\appendix
\include{distributions}
\include{paquetages}
\include{normale}
\include{khi2}
\include{solutions}

\bibliography{stat,actu,vg,r}

\cleartoverso

\input{colophon}

\cleartoverso

%% Page couverture arrière
\pagestyle{empty}
\input{couverture-arriere}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:

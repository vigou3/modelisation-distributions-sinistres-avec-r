%%% Copyright (C) 2021 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Modélisation des distributions de sinistres avec R»
%%% https://gitlab.com/vigou3/modelisation-distributions-sinistres-avec-r
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\chapter{Installation de paquetages dans \proglang{R}}
\label{chap:paquetages}

Plusieurs exercices de ce recueil requièrent l'utilisation du paquetage
\pkg{actuar} \citep{actuar}. Le paquetage doit être installé depuis le
site \emph{Comprehensive R Archive Network} (CRAN;
\url{https://cran.r-project.org}). Cette annexe explique comment
configurer \proglang{R} pour faciliter l'installation et
l'administration de paquetages externes.

Les instructions ci-dessous sont centrées autour de la création d'une
bibliothèque personnelle où seront installés les paquetages \proglang{R}
téléchargés de CRAN. Il est fortement recommandé de créer une telle
bibliothèque. Cela permet d'éviter d'éventuels problèmes d'accès en
écriture dans la bibliothèque principale et de conserver les paquetages
intacts lors des mises à jour de \proglang{R}. Nous montrons également
comment spécifier le site miroir de CRAN pour éviter d'avoir à le
répéter à chaque installation de paquetage.
\begin{enumerate}
\item Identifier le dossier de départ de l'utilisateur. En cas
  d'incertitude, examiner la valeur de la variable d'environnement
  \code{HOME}\footnote{%
    Pour les utilisateurs de GNU~Emacs sous Windows, la variable est
    créée par l'assistant d'installation de Emacs lorsqu'elle n'existe
    pas déjà.}, %
  depuis R avec la commande
\begin{Schunk}
\begin{Sinput}
> Sys.getenv("HOME")
\end{Sinput}
\end{Schunk}
  ou, pour les utilisateurs de Emacs, directement depuis l'éditeur avec
\begin{verbatim}
M-x getenv RET HOME RET
\end{verbatim}
  Nous référerons à ce dossier par le symbole \verb=~=.
\item Créer un dossier qui servira de bibliothèque de paquetages
  personnelle. Dans la suite, nous utiliserons \verb=~/R/library=.
\item Dans un fichier nommé \verb=~/.Renviron= (donc situé dans le
  dossier de départ), enregistrer la ligne appropriée ci-dessous:
\begin{verbatim}
R_LIBS_USER="~/R/library
\end{verbatim}
  Au besoin, remplacer le chemin \verb=~/R/library= par celui du
  dossier créé à l'étape précédente. Utiliser la barre oblique avant
  (\code{/}) dans le chemin pour séparer les dossiers.
\item Dans un fichier nommé \verb=~/.Rprofile=, enregistrer l'option
  suivante:
\begin{verbatim}
options(repos = "https://cloud.r-project.org")
\end{verbatim}
  Si désiré, remplacer la valeur de l'option \code{repos} par l'URL
  d'un autre site miroir de CRAN.

  Les utilisateurs de GNU~Emacs voudront ajouter une autre option. Le
  code à entrer dans le fichier \verb=~/.Rprofile= sera plutôt
\begin{verbatim}
options(repos = "https://cloud.r-project.org",
        menu.graphics = FALSE)
\end{verbatim}
\end{enumerate}
Consulter la rubriques d'aide de \code{Startup} pour les détails sur
la syntaxe et l'emplacement des fichiers de configuration, celles de
\code{library} et \code{.libPaths} pour la gestion des bibliothèques
et celle de \code{options} pour les différentes options reconnues par
\proglang{R}.

Après un redémarrage de \proglang{R}, la bibliothèque personnelle aura
préséance sur la bibliothèque principale et il ne sera plus nécessaire
de préciser le site miroir de CRAN lors de l'installation de paquetages.
Ainsi, la simple commande
\begin{Sinput}
> install.packages("actuar")
\end{Sinput}
téléchargera le paquetage \pkg{actuar} depuis de le miroir canadien de
CRAN et l'installera dans le dossier \verb=~/R/library=. Pour charger
le paquetage en mémoire, on fera
\begin{Sinput}
> library("actuar")
\end{Sinput}

On peut arriver au même résultat sans utiliser les fichiers de
configuration \code{.Renviron} et \code{.Rprofile}. Il faut cependant
recourir aux arguments \code{lib} et \code{repos} de la fonction
\code{install.packages} et à l'argument \code{lib.loc} de la fonction
\code{library}. Consulter les rubriques d'aide de ces deux fonctions
pour de plus amples informations.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "modelisation-distributions-sinistres-avec-r"
%%% coding: utf-8
%%% End:
